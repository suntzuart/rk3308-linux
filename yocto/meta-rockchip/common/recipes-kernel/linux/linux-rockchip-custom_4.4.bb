# Copyright (C) 2018 Fuzhou Rockchip Electronics Co., Ltd
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-kernel/linux/linux-yocto.inc
require linux-rockchip.inc

SRC_URI = " \
    git://${TOPDIR}/../../kernel;protocol=file;bareclone=1; \
    file://0001-Fix-yocto-compile-error.patch \
    file://0001-arm64-dts-rk3308-Allow-root-fstype-other-than-squash.patch \
"

SRC_URI_append_rockchip-rk3308-evb += " \
    file://cgroups.cfg \
    file://ext4.cfg \
    file://vt.cfg \
"

SRCREV = "be328c224dea1375078091cfdb375d219f32a3b1"
LINUX_VERSION = "4.4.132"

SRC_URI_append_rockchip-rk3308-evb += " file://cgroups.cfg file://ext4.cfg"

COMPATIBLE_MACHINE = ".*(rk3308|rk3326).*"
